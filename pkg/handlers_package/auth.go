package handlers

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/codekeep18feb/authentication/pkg/models_package"
	"gitlab.com/codekeep18feb/authentication/pkg/utils_package"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthHandler struct {
	collection *mongo.Collection
}

func NewAuthHandler(client *mongo.Client) *AuthHandler {
	collection := client.Database("my-mongo-container2").Collection("users")
	return &AuthHandler{
		collection: collection,
	}
}

func (h *AuthHandler) Signup(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	// Validate user data (e.g., check for required fields, validate email format)

	// Generate password hash
	hash, err := utils.GeneratePasswordHash(user.Password)
	if err != nil {
		log.Println(err)
		utils.RespondWithError(w, http.StatusInternalServerError, "Failed to generate password hash")
		return
	}

	// Store user in database
	user.Password = hash
	_, err = h.collection.InsertOne(context.TODO(), user)
	if err != nil {
		log.Println(err)
		utils.RespondWithError(w, http.StatusInternalServerError, "Failed to create user")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, user)
}

func (h *AuthHandler) Login(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var loginRequest models.LoginRequest
	err := json.NewDecoder(r.Body).Decode(&loginRequest)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	// Find user by email
	filter := bson.M{"email": loginRequest.Email}
	var user models.User
	err = h.collection.FindOne(context.TODO(), filter).Decode(&user)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnauthorized, "Invalid email or password")
		return
	}

	// Verify password
	if !utils.VerifyPassword(user.Password, loginRequest.Password) {
		utils.RespondWithError(w, http.StatusUnauthorized, "Invalid email or password")
		return
	}

	// Generate JWT token
	token, err := utils.GenerateJWTToken(user.ID.Hex())
	if err != nil {
		log.Println(err)
		utils.RespondWithError(w, http.StatusInternalServerError, "Failed to generate JWT token")
		return
	}

	utils.RespondWithJSON(w, http.StatusOK, map[string]string{"token": token})
}
