package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type User struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

type UserHandler struct {
	client *mongo.Client
}

func NewUserHandler(client *mongo.Client) *UserHandler {
	return &UserHandler{
		client: client,
	}
}

func (h *UserHandler) GetUsers(w http.ResponseWriter, r *http.Request) {
	collection := h.client.Database("my-mongo-container2").Collection("users")

	cursor, err := collection.Find(r.Context(), bson.M{})
	if err != nil {
		log.Println("Failed to retrieve users:", err)
		http.Error(w, "Failed to retrieve users", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(r.Context())

	var users []User
	if err := cursor.All(r.Context(), &users); err != nil {
		log.Println("Failed to decode users:", err)
		http.Error(w, "Failed to decode users", http.StatusInternalServerError)
		return
	}

	jsonResponse(w, http.StatusOK, users)
}

func (h *UserHandler) GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userID := params["id"]

	collection := h.client.Database("my-mongo-container2").Collection("users")

	var user User
	if err := collection.FindOne(r.Context(), bson.M{"_id": userID}).Decode(&user); err != nil {
		if err == mongo.ErrNoDocuments {
			http.Error(w, "User not found", http.StatusNotFound)
			return
		}

		log.Println("Failed to retrieve user:", err)
		http.Error(w, "Failed to retrieve user", http.StatusInternalServerError)
		return
	}

	jsonResponse(w, http.StatusOK, user)
}

func (h *UserHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userID := params["id"]

	collection := h.client.Database("my-mongo-container2").Collection("users")

	var updatedUser User
	if err := json.NewDecoder(r.Body).Decode(&updatedUser); err != nil {
		log.Println("Failed to decode request body:", err)
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	update := bson.M{"$set": bson.M{"name": updatedUser.Name, "email": updatedUser.Email}}
	if _, err := collection.UpdateOne(r.Context(), bson.M{"_id": userID}, update); err != nil {
		log.Println("Failed to update user:", err)
		http.Error(w, "Failed to update user", http.StatusInternalServerError)
		return
	}

	jsonResponse(w, http.StatusOK, updatedUser)
}

func (h *UserHandler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userID := params["id"]

	collection := h.client.Database("my-mongo-container2").Collection("users")

	if _, err := collection.DeleteOne(r.Context(), bson.M{"_id": userID}); err != nil {
		log.Println("Failed to delete user:", err)
		http.Error(w, "Failed to delete user", http.StatusInternalServerError)
		return
	}

	jsonResponse(w, http.StatusOK, map[string]string{"message": "User deleted successfully"})
}

func jsonResponse(w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Println("Failed to encode JSON response:", err)
		http.Error(w, "Failed to encode JSON response", http.StatusInternalServerError)
	}
}
